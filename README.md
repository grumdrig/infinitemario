Notch's Infinite Mario Bros
===========================

The code here is by Notch, [from whose site](http://www.mojang.com/notch/mario/) it was downloaded. The art
is by Nintendo. This readme is by me, [Grumdrig](https://bitbucket.org/grumdrig). I've only added a small
code change and the scripts (`*.sh`) in the root to get the game building and running for me.

The real readme to read is [Notch's README.txt](https://bitbucket.org/grumdrig/infinitemario/raw/ac4c1d43aefdb2058161a964c8e5965035f7b78c/README.txt),
so please take a look at it.
